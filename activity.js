db = db.getSiblingDB("hotel");


// Newly added document
db.rooms.insertMany(
[{
    "_id" : ObjectId("62e0cb825c97e1662e5bfb71"),
    "name" : "single",
    "accomodates" : 2.0,
    "price" : 1000.0,
    "description" : "A simple room with basic necessities",
    "room_available" : 10.0,
    "isAvalable" : false
},
// Newly added document
{
    "_id" : ObjectId("62e0cbe95c97e1662e5bfb77"),
    "name" : "double",
    "accomodates" : 3.0,
    "price" : 2000.0,
    "description" : "A simple room fit with the small family on vacation",
    "room_available" : 5.0,
    "isAvalable" : false
]
}
)

db.getCollection("room").find({});

db.room.findOne({"name":"double"})

db.room.updateOne({"isAvalable": false},{$set:{"isAvailable":false}})

db.room.deleteMany({"room_available":0})